(function ($, Drupal, drupalSettings) {
  "use strict";

  Drupal.behaviors.wordscrambles = {
    attach: function (context, settings) {
      let data = drupalSettings.gameBoxes.wordscramble;
      let words = data.scrambleWords;
      let wordLimit = data.wordLimit;
      let uniqueId = data.uniqueID;
      var $callcount = $(context).find('#words').once('wordscrambles');
      if ($callcount.length) {
      randomValues(words, wordLimit).map((word) =>
        WordFindGame.insertWordBefore($("#add-word-" + uniqueId).parent(), word)
      );
      }
      /* Init */
      function recreate(el) {
        var fillBlanks, game;
        try {
          game = new WordFindGame(el, {
            allowedMissingWords: 0,
            maxGridGrowth: 1,
            fillBlanks: fillBlanks,
            maxAttempts: 100,
          }, uniqueId);
        } catch (error) {
          $("#result-message")
            .text(`😞 ${error}, try to specify less ones`)
            .css({ color: "red" });
          return;
        }
        $(el).addClass("puzzle");
        $("#main").addClass("d-flex");
        $("#main").removeClass("hidden");
        window.game = game;
      }
      function randomValues(words, wordLimit) {
        let newArr = [];
        shuffleArray(words).forEach((element, index) => {
          if (index < wordLimit) {
            newArr.push(element);
          }
          return false;
        });
        return newArr;
      }
      function shuffleArray(array) {
        for (var i = array.length - 1; i > 0; i--) {
          // Generate random number
          var j = Math.floor(Math.random() * (i + 1));
          var temp = array[i];
          array[i] = array[j];
          array[j] = temp;
        }
        return array;
      }
      recreate("#puzzle-" + uniqueId);
      $("#wordscarmble-reset-" + uniqueId).click(function(){
        $("#wordscarmble-reset-" + uniqueId)
          .attr( "disabled" , "true" )
          .addClass("btn-disabled");
        $(".puzzle").removeAttr("style");
        $(".wordscramble__overlay").addClass("overlay-hidden");
        $('#words li:not(:last-child)').remove();
          randomValues(words, wordLimit).map((word) =>
          WordFindGame.insertWordBefore($("#add-word-" + uniqueId).parent(), word)
        );
        recreate("#puzzle-" + uniqueId);
      });
    },
  };
})(jQuery, Drupal, drupalSettings);
