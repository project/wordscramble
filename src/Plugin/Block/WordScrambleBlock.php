<?php

namespace Drupal\wordscramble\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Creates a block for 'Word Scramble' Game.
 *
 * @Block(
 *   id = "games_wordscramble",
 *   admin_label = @Translation("Word Scramble Block"),
 *   category = @Translation("Word Scramble Game"),
 * )
 */
class WordScrambleBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $form['#tree'] = TRUE;
    $config = $this->getConfiguration();
    $form['field_game_title'] = [
      '#type' => 'textfield',
      '#title' => 'Block Title',
      '#default_value' => isset($config['field_game_title']) ? $config['field_game_title'] : '',
    ];
    $form['field_game_instruction'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Instruction Prompt'),
      '#rows' => 5,
      '#default_value' => isset($config['field_game_instruction']) ? $config['field_game_instruction'] : '',
    ];
    $form['field_game_success'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Game Success Text'),
      '#rows' => 5,
      '#default_value' => isset($config['field_game_success']) ? $config['field_game_success'] : '',
    ];
    $form['word_Scramble_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => 'Scramble Words',
      '#prefix' => '<div id="word-Scramble-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];
    $num_names = $form_state->get('num_names');
    if (empty($num_names)) {
      $num_names = !empty($config['articles_alaune']) ? $config['articles_alaune'] : 1;
      $form_state->set('num_names', $num_names);
    }
    for ($i = 0; $i < $num_names; $i++) {
      $form['word_Scramble_fieldset'][$i] = [
        '#prefix' => '<div class="two-col">',
        '#suffix' => '</div>',
      ];
      $form['word_Scramble_fieldset'][$i]['word_scramble_game'] = [
        '#type' => 'textfield',
        '#maxlength' => 8,
        '#title' => 'Word',
        '#default_value' => isset($config['word_Scramble_fieldset'][$i]['word_scramble_game']) ? $config['word_Scramble_fieldset'][$i]['word_scramble_game'] : '',
      ];
    }
    $form['word_Scramble_fieldset']['actions']['add_item'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one'),
      '#submit' => [[$this, 'addmoreaddone']],
      '#ajax' => [
        'callback' => [$this, 'addmorecallback'],
        'wrapper' => 'word-Scramble-fieldset-wrapper',
      ],
    ];
    if ($num_names > 1) {
      $form['word_Scramble_fieldset']['actions']['remove_item'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove one'),
        '#submit' => [[$this, 'removeCallback']],
        '#ajax' => [
          'callback' => [$this, 'addmorecallback'],
          'wrapper' => 'word-Scramble-fieldset-wrapper',
        ],
      ];
    }
    $form['field_play_again'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Play Again'),
      '#description' => $this->t('Use this field to add Play again CTA to the game.'),
      '#default_value' => isset($config['field_play_again']) ? $config['field_play_again'] : '',
    ];
    $form['field_game_item_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Word Limit'),
      '#default_value' => isset($config['field_game_item_limit']) ? $config['field_game_item_limit'] : '',
    ];
    return $form;
  }

  /**
   * Implements function for ajax call back.
   */
  public function addmorecallback(array &$form, FormStateInterface $form_state) {
    // The form passed here is the entire form, not the subform that is
    // passed to non-AJAX callback.
    return $form['settings']['word_Scramble_fieldset'];
  }

  /**
   * Implements funciton for add field.
   */
  public function addmoreaddone(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_names');
    $add_button = $name_field + 1;
    $form_state->set('num_names', $add_button);
    $form_state->setRebuild();
  }

  /**
   * Implements funciton for remove field.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_names');
    if ($name_field > 1) {
      $remove_button = $name_field - 1;
      $form_state->set('num_names', $remove_button);
    }
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    unset($this->configuration['word_Scramble_fieldset']);
    foreach ($values['word_Scramble_fieldset'] as $key => $config_value) {
      if (is_numeric($key)) {
        $this->configuration['word_Scramble_fieldset'][$key]['word_scramble_game'] = $config_value['word_scramble_game'];
        $this->configuration['articles_alaune'] = $key + 1;
      }
    }
    $this->configuration['field_game_title'] = $values['field_game_title'];
    $this->configuration['field_game_instruction'] = $values['field_game_instruction'];
    $this->configuration['field_play_again'] = $values['field_play_again'];
    $this->configuration['field_game_item_limit'] = $values['field_game_item_limit'];
    $this->configuration['field_game_success'] = $values['field_game_success'];
    parent::blockSubmit($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $words = $config['word_Scramble_fieldset'];
    $success = $config['field_game_success'];
    $scramble_words = [];
    foreach ($words as $word) {
      $scramble_words[] = $word['word_scramble_game'];
    }
    $item_limit = !empty($config['field_game_item_limit']) ?
    $config['field_game_item_limit'] : 4;
    $title = $config['field_game_title'];
    $instruction = $config['field_game_instruction'];
    $play_again = $config['field_play_again'];
    $link = [];
    $uniqueid = $config['id'];
    return [
      '#theme' => 'game_box_wordscramble',
      '#data' => [
        'items' => $item_limit,
        'words' => $scramble_words,
        'instruction' => $instruction,
        'title' => $title,
        'link' => $link,
        'play_again' => $play_again,
        'success_text' => $success,
      ],
      '#uniqueID' => $uniqueid,
      '#attached' => [
        'library' => [
          'wordscramble/games_box_wordscramble',
        ],
        'drupalSettings' => [
          'gameBoxes' => [
            'wordscramble' => [
              'uniqueID' => $uniqueid,
              'scrambleWords' => $scramble_words,
              'wordLimit' => $item_limit,
            ],
          ],
        ],
      ],
    ];
  }

}
